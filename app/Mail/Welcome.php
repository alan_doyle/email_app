<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Welcome extends Mailable
{
    use Queueable, SerializesModels;

    public $mail;
    public $name;
    public $body;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mail, $name, $body)
    {
        $this->mail = $mail;
        $this->name = $name;
        $this->body = $body;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject('A Note')
                    ->markdown('emails.welcome', compact('mail','name','body'));
    }
}
