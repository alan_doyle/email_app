<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Send extends Model
{
    protected $fillable = [
        'name', 'body', 'email'
    ];
}
