<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Send;
use App\Mail\Welcome;
use Carbon\Carbon;

class SendController extends Controller
{
    public function index() 
    {
        $dt = Carbon::now();
        return view('/write', compact('dt'));
    }

    public function send() //accepts body from the comment controller
        {
            $this->validate(request(), [
                'name' => 'required',
                'body' => 'required',
                'email' => 'required|email',
            ]);  

            $mail = request('email');
            $name = request('name');
            $body = request('body');

            \Mail::to($mail)->send(new Welcome($mail, $name, $body));
            
            session()->flash('message', 'ecommend blood');
            return redirect('/');
        }

}