
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');

// window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

// const app = new Vue({
//     el: '#app'
// });


autosize($('textarea'));

$("#name").click(function () {
    $(".action").addClass("animated");
});


$(".dark").click(function () {
    $("body").toggleClass("darkmode");
});

// var editor = new MediumEditor('.editable', {
//     toolbar: {
//         /* These are the default options for the toolbar,
//            if nothing is passed this is what is used */
//         allowMultiParagraphSelection: true,
//         buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
//         diffLeft: 0,
//         diffTop: -10,
//         firstButtonClass: 'medium-editor-button-first',
//         lastButtonClass: 'medium-editor-button-last',
//         relativeContainer: null,
//         standardizeSelectionStart: false,
//         static: false,
//         /* options which only apply when static is true */
//         align: 'center',
//         sticky: false,
//         updateOnEmptySelection: false
//     }
// });

// $('.editable').bind('input propertychange', function () {
//     $("#post_" + $(this).attr("data-field-id")).val($(this).html());
// });
