<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Home - Take note</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/app.css">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
    <div class="intro">
            <div class="title">
                <h1>Take note</h1>
            </div>
            <div class="note">
                <p>No apps. No passwords. No Bullshit.</p>
            </div>
            <ul>
                <li>
                    <div class="item"><img src="svg/tipone.svg"></div>
                    <p>1. Take a note</p>
                </li>
                <li>
                    <div class="item"><img src="svg/tiptwo.svg"></div>
                    <p>2. Add your email</p>
                </li>
                <li>
                   <div class="item"><img src="svg/tipthree.svg"></div>
                    <p>3. Send</p>
                </li>
            <div class="actionup">
                <a href="/write">Write something</a>
            </div>
        </form>
    </div>
    </body>
</html>
