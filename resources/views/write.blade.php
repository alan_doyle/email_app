<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<html>
<head>
    <meta charset="UTF-8">
    <title>Write</title>

    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css">

    <script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Lekton|Roboto" rel="stylesheet"> 
    <link rel="stylesheet" href="css/app.css">
    <!-- <script src="https://cdn.jsdelivr.net/npm/medium-editor@latest/dist/js/medium-editor.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/medium-editor@latest/dist/css/medium-editor.min.css" type="text/css" media="screen" charset="utf-8"> -->
</head>



<body>
    <!-- <div class="logo">
        <a href="/"></a>
    </div> -->
    <div class="write">
        <span class="date">{{ $dt->format('l jS\\, M') }}</span>
        <form method="POST" action="/send" class="small_form">
            {{ csrf_field() }}
            <div class="title">
                <input  type="text" placeholder="Snazzy title" id="name" name="name" />
            </div>
            <div class="note">
                <textarea  placeholder="Write something for later.." id="body" name="body" ></textarea>
            </div>
            <div class="action">
                <input type="email" placeholder="Your email address..." id="email" name="email" />
                <button type="submit"> Send </button>
            </div>
        </form>
    </div>
    <div class="dark off">
        <a><img src="svg/dark.svg" alt="Kiwi standing on oval"></a>
    </div>
    @include ('partials.flash')
    @include ('partials.errors')
    <script src="https://cdn.jsdelivr.net/npm/autosize@4.0.2/dist/autosize.js"></script>
    <script src="js/app.js"></script>
    </body>
</html>
